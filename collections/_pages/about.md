---
title: "about"
layout: page
permalink: /about/
order: 1
---

Anthropogenics was created by me, Baird Langenbrunner, while I was procrastinating on my research and waiting for traffic to die down.  Please see my [zeroth post][post 000 link] for a bit more on my own experience that motivated me to create this website.  For more about my own scientific work, see my [research website][uci website].

My goal for this platform is to give graduate students and early-career scientists a place to write about the things that excite them.  More importantly, I hope that Anthropogenics will be viewed as an inclusive space that showcases the diversity of voices in the Earth sciences and adjacent disciplines.

While my background is primarily in climate science, I'm broadly interested in any topic concerned with how humans interact with the natural world.  I'm not trying to reinvent the wheel here, and there are a lot of online [resources][favorites link] that already do great work on these topics, though I think these platforms could do a better job at representing how young and diverse the field is.

If you like this project and want to see a specific topic or voice represented, please let me know (and tell others)!  Even better, if you're passionate about the same things and want to contribute, send me an email!

{:center: style="text-align: center"}

[Email me.][email]

{:center}

[post 000 link]: http://localhist:4000/archive/
[email]: mailto:langenbrunner@gmail.com
[uci website]: http://sites.uci.edu/baird
[favorites link]: http://localhost:4000/favorites/
